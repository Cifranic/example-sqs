# Purpose: 
Provide a demo of how SQS queues work.

## AWS Services used:
* EC2
* S3
* SQS 
* IAM

## How it works:
1. Receive a message from the queue contianing new filename
2. Use "aws s3" commmand to pull the filename from a bucket
3. Perform processing operations on the file
4. Move the processed file back into s3 bucket (in a processed folder)

## Where to run these scripts: 
* on an amazonLinux ec2 instance 

## IAM Roles for services instance: 
1. S3 read and write
2. SQS read and write 

### S3 buckets subfolders to create (in root of bucket):
1. processed-data
2. raw-data

