#!/bin/bash
# Purpose:  Send a SNS queue entry with name of a file within the Body
# Where to place this script:  Within a Lambda function 

# AWS REGION 
REGION="us-west-1"

# url of SQS queue
SQS_QUEUE="https://sqs.myqueue.amazonaws.com"

# get a random word for the message-body, append a .gz
FILE=$(echo $(curl -s https://random-word-api.herokuapp.com/word?number=1) | tr -d '"][')

FILE="${FILE}.gz"

echo "Populating queue with dummy data, inserting word ${FILE}"
# populate the queue with dummy data
aws sqs send-message --queue-url "${SQS_QUEUE}" --message-body "${FILE}" --region "${REGION}"

printf "\n==========================EXITING SEND TO QUEUE===============================\n\n"
