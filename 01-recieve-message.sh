#!/bin/bash

# Purpose: 
#   1. Receive a message from the queue contianing new filename
#   2. Use "aws s3" commmand to pull the filename from a bucket
#   3. Perform processing operations on the file
#   4. Move the processed file back into s3 bucket (in a processed folder)
# Where to run: Run this script in a cronjob within an ec2 instance 
# Roles for EC2 instance: 
#   1. S3 read and write
#   2. SQS read and write 
# S3 buckets subfolders to create (in root of bucket):
#   1. processed-data
#   2. raw-data

# name of your s3 bucket 
S3_BUCKET="my-bucket"

# url of SQS queue
SQS_QUEUE="https://sqs.myqueue.amazonaws.com"

# location to process the data
PROCESS_DIR="./process"

# region of ec2 instance 
REGION="us-west-1"

# Save output of SQS recieve-message command 
MESSAGE=$(aws sqs receive-message --queue-url "${SQS_QUEUE}" --attribute-names All --message-attribute-names All --max-number-of-messages 10 --region "${REGION}")

# check if message actaully returns anything
if [ -z "$MESSAGE" ]
then
    echo "Queue is empty; exiting!"
else
    echo "There is a message in the queue!"
    sleep 2s
    # Output the information 
    echo $MESSAGE | jq

    # get filename
    FILENAME=$(echo $MESSAGE | jq '.Messages | .[0].Body')
       
    # strip quotes from filename
    FILENAME=$(echo "$FILENAME" | tr -d '"')

    # remove .gz at end
    FILENAME=${FILENAME%???}

    # perform processing on the document 
    #  - create directory (if doesnt exist) to place processing files
    #  - Convert to read
    #  -
    echo "Performing processing on the document ${FILENAME}.gz"

    # make process directory if it doesnt exist
    mkdir -p $PROCESS_DIR 
    cd $PROCESS_DIR 


    ##### ONLY FOR TESTING 
    # create a file 
    
    echo $(date) > ./$FILENAME
    # create a dummy file with sample data

    # zip it up
    gzip -c ./$FILENAME > ./$FILENAME.gz
    
    # send it to raw-data folder
    aws s3 cp ./${FILENAME}.gz s3://"${S3_BUCKET}"/raw-data/ --region "${REGION}"
    
    
    sleep 5s
    # remove locally
    echo "DELETING FILES: $FILENAME $FILENAME.gz"
    rm -rf ./$FILENAME.gz
    rm -rf ./$FILENAME
    
    #
    #####

    # Pull the .gz file from S3 buicket 
    echo "Pulling file ${FILENAME}.gz from bucket ${S3_BUCKET}"

    # pull the file from s3 bucket 
    aws s3 cp s3://"${S3_BUCKET}"/raw-data/"${FILENAME}".gz . --region "${REGION}"
    
    

    # File comes in as a .gz ... if file exsits, then move forward and process
    if [ -f "$FILENAME.gz" ]; then
        echo "$FILENAME.gz downloaded from S3 exists."

        # Decompress the initial data
        gzip -d $FILENAME.gz

        # Read the hidden characters using cat
        cat -ETv $FILENAME | sed "s/\^\^/,/g" > "${FILENAME}_processed.csv"

        # zip up processed data 
        zip "${FILENAME}_processed.csv.zip" "${FILENAME}_processed.csv"

        # Upload processed data (in zip format) to s3 bucket:
        aws s3 cp "${FILENAME}_processed.csv.zip" s3://"${S3_BUCKET}"/processed-data/ --region "${REGION}"


    else # something went wrong and the file could not be pulled from s3
        echo "Something went wrong and the file, $FILENAME could not be pulled from s3... $FILENAME does not exist."
    fi

    # Decompress gz file into txt file, store into process directory
    # parse http response from the sqs recieve-messsage command for the RecieptHandle
    #    NOTE:ReciptHandle is used to delete the message from the queue  
    RECEIPT=$(echo $MESSAGE | jq '.Messages | .[0].ReceiptHandle')

    # delete contents of processing directory

    # remove quotes 
    RECEIPT=$(echo "$RECEIPT" | tr -d '"')

    echo "Deleting message in queue that contains document ${FILENAME}"
    # delete the latest on the queue
    aws sqs delete-message --queue-url "${SQS_QUEUE}" --receipt-handle "${RECEIPT}" --region "${REGION}"
fi






