#!/bin/bash

# url of SQS queue
SQS_QUEUE="https://sqs.myqueue.amazonaws.com/"

# region of ec2 instance 
REGION="us-west-1"


# Deletes ALL data within the queue
aws sqs purge-queue --queue-url "${SQS_QUEUE}" --region "${REGION}"
